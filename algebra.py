class Constant:
    def __init__(self, name, scaling=0):
        self.name = name
        self.scaling = scaling

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)


class Momentum:
    def __init__(self, p, coeff=1, scaling=0):
        self.p = p
        self.coeff = coeff
        self.scaling = scaling

    def shift1(self, **rules):
        try:
            return [
                self.__class__(r.p, r.coeff * self.coeff, r.scaling)
                for r in rules[self.p]
            ]
        except KeyError:
            return [self]

    def __str__(self, flag=""):
        if self.coeff == 1:
            if self.scaling != 0:
                return f"x^{self.scaling} * {self.p}{flag}"
            else:
                return self.p + flag
        elif self.coeff == -1:
            if self.scaling != 0:
                return f"-x^{self.scaling} * {self.p}{flag}"
            else:
                return f"-{self.p}{flag}"
        else:
            if self.scaling != 0:
                return f"x^{self.scaling} * {self.coeff} * {self.p}{flag}"
            else:
                return f"{self.coeff} * {self.p}{flag}"

    def __neg__(self):
        return self.__class__(self.p, -self.coeff, self.scaling)

    def __hash__(self):
        return hash((self.__class__.__name__, self.coeff, self.p))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __mul__(self, a):
        return self.__class__(self.p, a * self.coeff, self.scaling)

    def __rmul__(self, a):
        return self.__mul__(a)

    def plus(self, scaling=None):
        return Plus(self.p, self.coeff, scaling if scaling else self.scaling)

    def minus(self, scaling=None):
        return Minus(self.p, self.coeff, scaling if scaling else self.scaling)

    def perp(self, scaling=None):
        return Perp(self.p, self.coeff, scaling if scaling else self.scaling)

    def lightcone(self, scaling=[0, 0, 0]):
        if scaling[0] is not None:
            yield self.plus(scaling[0])
        if scaling[1] is not None:
            yield self.minus(scaling[1])
        if scaling[2] is not None:
            yield self.perp(scaling[2])

    def __repr__(self):
        return str(self)


class Plus(Momentum):
    def __str__(self):
        return super().__str__("+")


class Minus(Momentum):
    def __str__(self):
        return super().__str__("-")


class Perp(Momentum):
    def __str__(self):
        return super().__str__("T")


class SP:
    onshell = {}

    @classmethod
    def build(cls, i, j, coeff=1):
        if isinstance(i, Momentum):
            i = [i]
        if isinstance(j, Momentum):
            j = [j]

        return [cls._build1(a, b, coeff) for a in i for b in j]

    @classmethod
    def _build1(clc, i, j, coeff=1):
        if isinstance(i, Plus) and isinstance(j, Plus):
            return None, 0
        if isinstance(i, Minus) and isinstance(j, Minus):
            return None, 0
        if isinstance(i, Plus) and isinstance(j, Perp):
            return None, 0
        if isinstance(i, Minus) and isinstance(j, Perp):
            return None, 0
        if isinstance(i, Perp) and isinstance(j, Plus):
            return None, 0
        if isinstance(i, Perp) and isinstance(j, Minus):
            return None, 0

        if hash(i.p) > hash(j.p):
            i, j = j, i

        try:
            return clc.onshell[i, j], 1
        except KeyError:
            pass

        self = clc()
        self.i = i.__class__(i.p, scaling=i.scaling)
        self.j = j.__class__(j.p, scaling=j.scaling)

        if i.coeff != 1:
            coeff *= i.coeff
        if j.coeff != 1:
            coeff *= j.coeff

        return self, coeff

    @property
    def scaling(self):
        return self.i.scaling + self.j.scaling

    def __str__(self):
        return f"({self.i}).({self.j})"

    def __hash__(self):
        return hash((self.i, self.j))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __repr__(self):
        return str(self)


class Propagator:
    def __init__(self, ps, m):
        self.ps = ps
        self.m = m
        self.terms = None

    @property
    def momenta(self):
        if self.terms is None:
            for i in self.ps:
                yield i
        else:
            for term in self.terms.keys():
                if isinstance(term, SP):
                    yield term.i
                    yield term.j

    def coeff(self, q):
        for p in self.ps:
            if p.__class__ == q.__class__ and p.p == q.p:
                return p.coeff
        return 0

    def without(self, qs):
        for p in self.ps:
            if any(p.__class__ == q.__class__ and p.p == q.p for q in qs):
                continue
            yield p

    def shifted(self, **rules):
        psnew = [j for p in self.ps for j in p.shift1(**rules)]
        return Propagator(psnew, self.m)

    def expand(self):
        sps = SP.build(self.ps, self.ps)
        if self.m != 0:
            sps += [(self.m, -1)]
        self.terms = {}

        for t, coeff in sps:
            try:
                self.terms[t] += coeff
            except KeyError:
                self.terms[t] = coeff

        self.terms = {t: c for t, c in self.terms.items() if c != 0}
        return self

    def scaling(self):
        self.expand()
        return min(s.scaling for s in self.terms.keys())

    def __str__(self):
        if self.terms is not None:
            return " + ".join(f"{c} * ({i})" for i, c in self.terms.items())
        else:
            return f"({' + '.join(str(i) for i in self.ps)})^2 - {self.m}"

    def __repr__(self):
        return str(self)


if __name__ == "__main__":
    p1 = Momentum("p1", scaling=1)
    p2 = Momentum("p2", scaling=2)
    m = Constant("m")

    SP.onshell[p1, p1] = m

    expr = Propagator([p1, -p2], m)
    print(str(expr))
    expr.expand()
    print(str(expr))
    print(expr.scaling())
