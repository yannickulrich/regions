from algebra import *
from regions import *


k1 = Momentum("k1")
k2 = Momentum("k2")
p1 = Momentum("p1")
p2 = Momentum("p2")
p3 = Momentum("p3")
p4 = Momentum("p4")
m = Constant("m", scaling=1)

elims = all_elims([p1, p2, p3, p4])
frames = list(b2b_m0_frame([p1, p2, p3, p4]))

regs1 = [(0, 0, 1, 1), (0, 1, 1, 0), (0, -1, -1, 0), (0, 0, -1, -1), (0, 0, 0, 0)]
I1 = Integral([k1], [[k1, p1], [k1, p1, p2], [k1, -p3], [k1]], [m, m, m, m])
for v, (s, f, r) in I1.identify_regions(regs1, frames, {"p4": [-p1, -p2, -p3]}):
    print(v, (s, f, r))

regs2 = [
    (0, 0, 1, 0, 0, 1, 1),
    (0, 0, 1, 1, 0, 0, 1),
    (0, 0, 1, 1, 1, 0, 0),
    (0, 0, 1, 1, 1, 1, 1),
    (0, 1, 1, 1, 0, 0, 0),
    (0, -1, -1, -1, -1, 0, 0),
    (0, -1, -1, -1, 0, 0, -1),
    (0, -1, -1, 0, -1, -1, 0),
    (0, -1, -1, 0, 0, 0, 0),
    (0, 0, -1, -1, -1, 0, -1),
    (0, 0, 0, -1, -1, 0, 0),
    (0, 0, 0, 0, -1, -1, 0),
    (0, 0, 0, 0, 0, 0, 0),
]
I2 = Integral(
    [k1, k2],
    [
        [k1, p2],
        [k1],
        [k1, -p1],
        [k1, k2, -p1],
        [k1, k2, p2, p4],
        [k1, k2, p2],
        [k2],
    ],
    [m, m, m, m, m, m, 0],
)

for v, (s, f, r) in I2.identify_regions(regs2, frames, {"p4": [-p1, -p2, -p3]}):
    print(v, (s, f, r))
