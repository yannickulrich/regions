from algebra import *
from itertools import combinations, product
import numpy as np


def b2b_m0_frame(momenta):
    momenta = [p if isinstance(p, str) else p.p for p in momenta]
    for i, j in combinations(momenta, 2):
        dic = {p: (0, 0, 0) for p in momenta}
        dic[i] = (None, 0, None)
        dic[j] = (0, None, None)
        yield dic


def all_elims(momenta):
    ans = {}
    for p in momenta:
        ans[p.p] = [-q for q in momenta if p != q]
    return ans


def solve(A, B):
    if len(B) == 1:
        return [([-1 // A[0, 0]], [1 // A[0, 0]])]
    if len(B) == 2:
        det = A[0, 0] * A[1, 1] - A[0, 1] * A[1, 0]
        return [
            ([-A[1, 1] // det, +A[0, 1] // det], [+A[1, 1] // det, -A[0, 1] // det]),
            ([+A[1, 0] // det, -A[0, 0] // det], [-A[1, 0] // det, +A[0, 0] // det]),
        ]
    if len(B) == 3:
        det = (
            -A[0, 2] * A[1, 1] * A[2, 0]
            + A[0, 1] * A[1, 2] * A[2, 0]
            + A[0, 2] * A[1, 0] * A[2, 1]
            - A[0, 0] * A[1, 2] * A[2, 1]
            - A[0, 1] * A[1, 0] * A[2, 2]
            + A[0, 0] * A[1, 1] * A[2, 2]
        )

        return [
            (
                [
                    (+A[1, 2] * A[2, 1] - A[1, 1] * A[2, 2]) // det,
                    (-A[0, 2] * A[2, 1] + A[0, 1] * A[2, 2]) // det,
                    (+A[0, 2] * A[1, 1] - A[0, 1] * A[1, 2]) // det,
                ],
                [
                    (-A[1, 2] * A[2, 1] + A[1, 1] * A[2, 2]) // det,
                    (+A[0, 2] * A[2, 1] - A[0, 1] * A[2, 2]) // det,
                    (-A[0, 2] * A[1, 1] + A[0, 1] * A[1, 2]) // det,
                ],
            ),
            (
                [
                    (-A[1, 2] * A[2, 0] + A[1, 0] * A[2, 2]) // det,
                    (+A[0, 2] * A[2, 0] - A[0, 0] * A[2, 2]) // det,
                    (-A[0, 2] * A[1, 0] + A[0, 0] * A[1, 2]) // det,
                ],
                [
                    (+A[1, 2] * A[2, 0] - A[1, 0] * A[2, 2]) // det,
                    (-A[0, 2] * A[2, 0] + A[0, 0] * A[2, 2]) // det,
                    (+A[0, 2] * A[1, 0] - A[0, 0] * A[1, 2]) // det,
                ],
            ),
            (
                [
                    (+A[1, 1] * A[2, 0] - A[1, 0] * A[2, 1]) // det,
                    (-A[0, 1] * A[2, 0] + A[0, 0] * A[2, 1]) // det,
                    (+A[0, 1] * A[1, 0] - A[0, 0] * A[1, 1]) // det,
                ],
                [
                    (-A[1, 1] * A[2, 0] + A[1, 0] * A[2, 1]) // det,
                    (+A[0, 1] * A[2, 0] - A[0, 0] * A[2, 1]) // det,
                    (-A[0, 1] * A[1, 0] + A[0, 0] * A[1, 1]) // det,
                ],
            ),
        ]

    raise NotImplemented


class Integral:
    all_regions = ["c", "-c", "s", "h"]

    @staticmethod
    def _normalise(it):
        if not hasattr(it, "__next__"):
            it = iter(it)

        v0 = next(it)

        def i(it):
            yield 0
            yield from (j - v0 for j in it)

        return tuple(i(it))

    @staticmethod
    def region2scaling(r):
        if isinstance(r, tuple) and len(r) == 3:
            return r

        if r == "h":
            return (0, 0, 0)
        elif r == "c":
            return (2, 0, 1)
        elif r == "-c":
            return (0, 2, 1)
        elif r == "s":
            return (1, 1, 1)
        elif r == "us":
            return (2, 2, 2)
        elif r == "uc":
            return (4, 2, 3)
        raise NotImplemented

    def __init__(self, k, momenta=None, masses=None, props=None):
        self.k = k
        if momenta and masses:
            self.props = [Propagator(p, m) for p, m in zip(momenta, masses)]
        elif props:
            self.props = props

        self.momenta = set()
        for prop in self.props:
            self.momenta.update(prop.momenta)

    def list_shifts(self):
        for props in combinations(self.props, len(self.k)):
            A = np.array([[prop.coeff(k) for k in self.k] for prop in props], dtype=int)
            B = [list(prop.without(self.k)) for prop in props]

            if np.isclose(np.linalg.det(A), 0):
                continue

            sol = solve(A, B)

            yield {
                k.p: [j * c for p, c in zip(B, lb) for j in p]
                + [p * c for p, c in zip(self.k, la)]
                for k, (lb, la) in zip(self.k, sol)
            }

    def shifted(self, rules):
        return self.__class__(
            self.k, props=[prop.shifted(**rules) for prop in self.props]
        )

    def lightcone_props(self, scalings={}):
        pmap = {}
        for p in self.momenta:
            try:
                pmap[p] = list(p.lightcone(scalings[p.p]))
            except KeyError:
                pmap[p] = list(p.lightcone())

        return [
            Propagator([i for p in prop.ps for i in pmap[p]], prop.m)
            for prop in self.props
        ]

    def expand(self, region, frame):
        scalings = {}
        assert len(region) == len(self.k)
        for k, r in zip(self.k, region):
            scalings[k.p] = self.region2scaling(r)

        scalings.update(frame)

        return self.lightcone_props(scalings)

    def expand_all(self, frames):
        for frame in frames:
            for region in product(self.all_regions, repeat=len(self.k)):
                props = self.expand(region, frame)
                yield (frame, region, self._normalise(p.scaling() for p in props))

    def shift_all(self, eliminations={}):
        for shift in self.list_shifts():
            for elim, rhs in eliminations.items():
                dic = {elim: rhs}
                dic.update(shift)
                I = self.shifted(dic)
                yield dic, I

    def identify_regions(self, vecs, frames, eliminations):
        vecs = set(self._normalise(v) for v in vecs)

        frames = list(frames)

        for shift, I in self.shift_all(eliminations):
            for frame, reg, vec in I.expand_all(frames):
                if vec in vecs:
                    yield (vec, (shift, frame, reg))


if __name__ == "__main__":
    k1 = Momentum("k1")
    p1 = Momentum("p1")
    p2 = Momentum("p2")
    p3 = Momentum("p3")
    m = Momentum("m")

    I = Integral([k1], [[k1, p1], [k1, p1, p2], [k1, -p3], [k1]], [m, m, m, m])
    for s in I.list_shifts():
        print(" + ".join(str(i) for i in s["k1"]))
        print("\n".join(str(i) for i in I.shifted(s).props))
